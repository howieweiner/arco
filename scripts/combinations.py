#!/usr/bin/python

import csv

def main():
    input_file = 'singles_original.csv'
    output_file = 'combinations.csv'

    dataReader = csv.reader(open(input_file), delimiter=',', quotechar='"')

    fabrics = {}

    count = 0
    for row in dataReader:
        if count < 3:
            # first column is fabric
            fabric_name = row[0]

            # ignore header row
            if fabric_name != 'fabric':
                # remove first column
                row_data = row[1:]

                print fabric_name

                # now group every set of 4 columns
                groups = groupData(row_data)
                fabrics[fabric_name] = groups

            count += 1


    for fabric in fabrics:
        print fabric

def groupData(data):
    for i in xrange(0, len(data), 4):
        yield data[i:i+4]

if __name__ == "__main__":
    main()