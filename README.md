This project uses Grunt to build the release and bower to install dependencies.

#App dependencies 
To build the app dependencies, please do the following:

1. Ensure that node package manager and bower is installed (e.g. using Homebrew on a Mac)
1. Install node packages - (sudo) npm install
1. Install bower pacakges bower install

#Running the app
To run the app using the local webserver, run:

    grunt
    
This will launch a local web server on port 8000. The app can be accessed at http://localhost:8000/src

#Building the app
To build a distribution for the app, run:

    grunt dist
    
This will build a version of the app with concatenated, minified and timestamped assets. The resulting build will be 
copied into the /dist/ folder. these files should be placed on the webserver
    


