'use strict';

module.exports = function (grunt) {
  // load all grunt tasks matching the `grunt-*` pattern
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        'src/**/*.js'
      ]
    },

    concat: {
    },

    copy: {
      html: {
        files: [
          { cwd: 'src', src: ['*.html'], dest: 'dist/', expand: true }
        ]
      },
      assets: {
        files: [
          { cwd: 'src', src: ['fonts/**'], dest: 'dist/', expand: true },
          { cwd: 'src', src: ['images/**'], dest: 'dist/', expand: true },
          { cwd: 'src', src: ['data/**'], dest: 'dist/', expand: true }
        ]
      }
    },

    uglify: {
      options: {
        mangle: false
      }
    },

    ngAnnotate: {
      options: {
      },
      app: {
        files: {
          'dist/js/app.js': ['dist/js/app.js']
        }
      }
    },

    useminPrepare: {
      html: {
        src: ['src/index.html', 'src/products.html']
      },
      options: {
        dest: 'dist'
      }
    },

    usemin: {
      html: {
        src: ['dist/index.html', 'dist/products.html']
      }
    },

    ngtemplates: {
      app: {
        cwd: 'src',
        src: 'app/**/*.html',
        dest: 'dist/js/tpl.js',
        options: {
          usemin: 'js/app.js' // <~~ This came from the <!-- build:js --> block
        },
        htmlmin: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true,
          removeComments: true, // Only if you don't use comment directives!
          removeEmptyAttributes: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true
        }
      }
    },

    cssmin: {
    },

    filerev: {
      options: {
        algorithm: 'md5',
        length: 8
      },
      files: {
        src: [
          'dist/js/**/*.js',
          'dist/css**/*.css'
        ]
      }
    },

    clean: {
      dist: ['dist']
    },

    watch: {
      options: {
        atBegin: true,
        livereload: true
      },
      dev: {
        files: ['Gruntfile.js', 'src/**/*.html', 'src/app/**/*.js'],
        tasks: []
      }
    },

    connect: {
      server: {
        options: {
          port: 8000,
          host: 'localhost'
        }
      }
    }
  });

  /**
   * Clean the dist folder.
   * Run an annotation check over the angular code
   * Copy index.html to dist folder
   * Run usemin to replace inline js and css src paths to single files
   *  - compile angular templates into cache as part of usemin
   *  - concat files
   *  - minify css
   *  - uglify js code, but don't mangle as Angular seems to have issues with this.
   *  - and file revisions to new files
   *
   */
  grunt.registerTask('build', [
    'clean',
    'copy',
    'ngAnnotate',
    'useminPrepare',
    'ngtemplates',
    'concat',
    'cssmin',
    'uglify',
    'filerev',
    'usemin'
  ]);

  grunt.registerTask('dev', ['connect:server', 'watch:dev']);
  grunt.registerTask('dist', ['jshint', 'build']);
  grunt.registerTask('default', ['dev']);
};