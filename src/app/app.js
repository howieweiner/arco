(function () {
  'use strict';

  angular
    .module('app', [
      // app modules
      'app.core',
      'app.config',
      'app.selector',
      'app.filters',
      'app.services',
      'app.directives'
    ]);
})();