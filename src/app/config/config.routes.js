(function () {
  'use strict';

  angular
    .module('app.config')
    .config(routerConfig);

  routerConfig.$inject = ['$urlRouterProvider'];

  /* @ngInject */
  function routerConfig($urlRouterProvider) {

    $urlRouterProvider.when("", "/select");
    $urlRouterProvider.when("/", "/select");
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/select');
  }
})();