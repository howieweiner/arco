(function () {
  'use strict';

  angular
    .module('app.selector')
    .config(routerConfig);

  routerConfig.$inject = ['$stateProvider'];

  /* @ngInject */
  function routerConfig($stateProvider) {

    $stateProvider

      .state('products', {
        url: "/select",
        views: {
          main: {
            controller: 'ProductCtrl as products',
            templateUrl: 'app/selector/products.html',
            resolve: {
              productSelector: 'ProductSelector',
              productWizard: 'ProductWizard',
              context: function (productWizard) {
                return {
                  pageHeading: function () {

                    var lyr = productWizard.selection.currentLayer,
                        txt;

                    if(lyr === 'outer') {
                      txt = 'Select an outer layer';
                    }
                    else {
                      txt = 'Select a ' + lyr + ' layer';
                    }
                    return txt;
                  }
                };
              }
            }
          },
          menu: {
            controller: 'MenuCtrl as menu',
            templateUrl: 'app/selector/menu.html'
          }
        }
      })

      .state('results', {
        url: "/results",
        views: {
          main: {
            templateUrl: 'app/selector/results.html',
            controller: 'ResultsCtrl as results',
            resolve: {
              context: function () {
                return {
                  pageHeading: 'Test Results Summary'
                };
              }
            }
          },
          menu: {
            controller: 'MenuCtrl as menu',
            templateUrl: 'app/selector/menu.html'
          }
        }
      });
  }
})();