(function () {
  'use strict';

  angular
    .module('app.selector')
    .controller('LayoutCtrl', LayoutCtrl)
    .controller('ProductCtrl', ProductCtrl)
    .controller('MenuCtrl', MenuCtrl)
    .controller('ResultsCtrl', ResultsCtrl)
  ;

  LayoutCtrl.$inject = ['$scope'];
  ProductCtrl.$inject = ['$scope', '$rootScope', 'context', 'ProductSelector', 'ProductWizard', 'smoothScroll'];
  MenuCtrl.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'ProductWizard', 'smoothScroll'];
  ResultsCtrl.$inject = ['$scope', 'context', 'ProductWizard', 'smoothScroll'];

  /* @ngInject */
  function LayoutCtrl($scope) {
    $scope.setcontext = function (context) {
      $scope.context = context;

      // set the view context onto the topmost layout controller
      $scope.layout.context = context;
    };
  }

  /* @ngInject */
  function ProductCtrl($scope, $rootScope, context, productSelector, productWizard, smoothScroll) {
    var vm = this;
    $scope.setcontext(context);

    // show loading overlay until data loaded
    vm.loading = true;

    // wait for promise before continuing, as service needs to initialise
    productSelector.init().then(function () {
      vm.allProducts = productSelector.allProducts();
      vm.loading = false;
    });

    vm.current = productWizard.current();
    vm.single = productWizard.singleResults();
    vm.combined = productWizard.combinedResults();
    vm.selection = productWizard.selection;

    vm.productCount = null;

    vm.slickConfig = {
      slide: 'li',
      autoplay: false,
      dots: true,
      infinite: false,
      speed: 800,
      slidesToShow: 4,
      slidesToScroll: 4,
      centerPadding: 0,
      responsive: [
        {
          breakpoint: 960,
          settings: {
            arrows: false,
            slidesToShow: 3,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
      ]
    };

    $scope.carouselHandler = {};

    vm.onCarouselInit = function () {
      // when we first show the carousel, only show base products
      $scope.carouselHandler.slickFilter('.base');
    };

    // watch for change to currentLayer, so we can filter the products
    $scope.$watch(function () {
      return productWizard.selection.currentLayer;
    }, function (newLayer, oldLayer) {

      if (oldLayer !== newLayer) {
        // remove selected and disabled css from all/current elements
        var allSlides = document.getElementsByClassName("product");

        angular.element(allSlides).removeClass('selected');
        angular.element(allSlides).removeClass('disabled');

        // if we have at least one product/fabric already selected, then we may need to check for valid combinations
        // if we are now viewing mid or outer layer, filter any invalid products based on combinations
        var invalidProductCodes = productWizard.getInvalidProductCodesForCurrentLayer();

        $scope.carouselHandler.slickFilter(function (index, slide) {
          var theSlide = angular.element(slide),
            displayThis;

          // filter out products not selectable for the current layer
          displayThis = theSlide.hasClass(newLayer);

          // ensure any selected products are also filtered from the carousel, as products can be used for more than
          // one layer
          if (displayThis) {
            if (vm.selection.base !== null) {
              displayThis = !theSlide.hasClass('p' + vm.selection.base.code);
            }
            if (vm.selection.mid !== null) {
              displayThis = !theSlide.hasClass('p' + vm.selection.mid.code);
            }
            if (vm.selection.outer !== null) {
              displayThis = !theSlide.hasClass('p' + vm.selection.outer.code);
            }
          }

          if (displayThis && invalidProductCodes !== null) {
            // extract product code form class name. IE doesn't play nicely with data attributes
            var classes = theSlide.attr('class').toString().split(' ');
            var productCode;
            angular.forEach(classes, function (className) {
              if (className.indexOf('p') === 0 && className !== 'product') {
                productCode = className.substring(1); // remove 'p;' prefix
              }
            });

            if (productCode !== undefined) {
              return !_.contains(invalidProductCodes, productCode);
            }
            else {
              return false; // shouldn't get here
            }
          }

          return displayThis;
        });

        $scope.carouselHandler.slickGoTo(0);

        var top = document.getElementById('top');
        smoothScroll(top);
      }
    });

    // When a product is added  to the wizard, or the wizard is reset by the select directive, a message is broadcast,
    // so that the controller can initiate a scope digest()
    $scope.$on('selectionChanged', function () {
      // determine class for product images based on number of selected products
      var count = productWizard.productCount();
      vm.productCountClass = determineProductCountClass(count);

      vm.productCount = count;
      vm.current = productWizard.current();
      vm.single = productWizard.singleResults();
      vm.combined = productWizard.combinedResults();
      $scope.$digest();

      $rootScope.noFinalResults = productWizard.noFinalResults();
    });

    $scope.$on('selectionSkipped', function () {
      var count = productWizard.productCount();
      vm.productCountClass = determineProductCountClass(count);

      vm.productCount = count;
      vm.current = productWizard.current();
      vm.single = productWizard.singleResults();
      vm.combined = productWizard.combinedResults();

      $rootScope.noFinalResults = productWizard.noFinalResults();

    });

    $scope.$on('selectionReset', function () {
      // determine class for product images based on number of selected products
      var count = productWizard.productCount();
      vm.productCountClass = determineProductCountClass(count);

      vm.current = productWizard.current();
      vm.single = productWizard.singleResults();
      vm.combined = productWizard.combinedResults();
    });
  }

  /* @ngInject */
  function MenuCtrl($scope, $rootScope, $state, $timeout, productWizard, smoothScroll) {
    var vm = this;
    var top = document.getElementById('top');

    vm.wizard = productWizard;
    $rootScope.noFinalResults = false;

    vm.restart = function () {
      productWizard.restart();
      updateContext();
      $state.go('products');
      $rootScope.noFinalResults = productWizard.noFinalResults();
    };

    vm.skip = function (event) {
      // if there are no valid results, do nothing
      if ($rootScope.noFinalResults) {
        event.preventDefault();
        return;
      }

      // if a product is selected, then clear. need a timeout so that carousel doesn't load before clearing - purely cosmetic
      if (productWizard.current() !== null) {
        productWizard.skip();
        $rootScope.noFinalResults = productWizard.noFinalResults();
        smoothScroll(top);

        // if there are no products for outer layer, go straight to summary
        if (productWizard.selection.noSelectableOuterLayer) {
          productWizard.selection.complete = true;
          $state.go('results');
        }

        $timeout(function () {
          productWizard.next();
          if (productWizard.complete()) {
            $state.go('results');
          }
          updateContext();
          $rootScope.noFinalResults = productWizard.noFinalResults();
        }, 2000);
      }
      else {
        productWizard.next();
        $rootScope.noFinalResults = productWizard.noFinalResults();
        smoothScroll(top);
        if (productWizard.complete()) {
          $state.go('results');
        }
        updateContext();
      }
    };

    vm.next = function (event) {
      // if there are no valid results, do nothing
      if ($rootScope.noFinalResults) {
        event.preventDefault();
        return;
      }

      // if there are no products for outer layer, go straight to summary
      if (productWizard.selection.noSelectableOuterLayer) {
        productWizard.selection.complete = true;
        $state.go('results');
      }

      productWizard.next();
      $rootScope.noFinalResults = productWizard.noFinalResults();
      smoothScroll(top);

      if (productWizard.complete()) {
        $state.go('results');
      }
      updateContext();
    };

    function updateContext() {
      var lyr = productWizard.selection.currentLayer,
        txt;

      if (lyr === 'outer') {
        txt = 'Select an outer layer';
      }
      else {
        txt = 'Select a ' + lyr + ' layer';
      }
      $scope.layout.context.pageHeading = txt;
    }

    $scope.$on('selectionChanged', function () {
      $scope.$apply(function () {
        $rootScope.noFinalResults = productWizard.noFinalResults();
      });
    });
  }

  /* @ngInject */
  function ResultsCtrl($scope, context, productWizard, smoothScroll) {
    var vm = this;
    $scope.setcontext(context);
    vm.combined = productWizard.combinedResults();
    vm.selection = productWizard.selection;

    // determine class for product images based on number of selected products
    var count = productWizard.productCount();
    vm.productCountClass = determineProductCountClass(count);

    var top = document.getElementById('top');
    smoothScroll(top);
  }

  /**
   * Utility to determine class based on number of products - used for large images
   * @param count
   */
  function determineProductCountClass(count) {
    if (count === 1) {
      return 'one-product';
    }
    else if (count === 2) {
      return 'two-products';
    }
    else if (count === 3) {
      return 'three-products';
    }
    else {
      return null;
    }
  }
})();