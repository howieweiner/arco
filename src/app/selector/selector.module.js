(function () {
  'use strict';

  angular.module('app.selector', ['app.services', 'app.directives', 'bardo.directives']);
})();