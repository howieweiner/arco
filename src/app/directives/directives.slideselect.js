(function () {
  'use strict';

  angular
    .module('app.directives')
    .directive('slideSelect', SlideSelect)
  ;

  SlideSelect.$inject = ['ProductWizard'];

  function SlideSelect(productWizard) {

    return {
      restrict: 'A',

      scope: {
        product: '@'
      },

      link: function (scope, element, attrs) {
        element.bind('click', function (event) {

          // remove selected css from all/current elements
          var allSlides = document.getElementsByClassName("product");
          angular.element(allSlides).removeClass('selected');
          // add css selected class to the element
          element.addClass('selected');

          // update wizard with selected product
          productWizard.addProduct(attrs.product);

          // detect if there no valid products for the final layer
          if (productWizard.selection.currentLayer === 'mid') {
            // wrap in apply so that MenuCtrl picks up change to wizard attributes
            scope.$apply(function () {
              productWizard.determineIfSelectableOuterLayer();
            });
          }
        });
      }
    };
  }
})();