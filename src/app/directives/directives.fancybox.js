(function () {
  'use strict';

  angular
    .module('app.directives')
    .directive('fancybox', Fancybox)
  ;

  function Fancybox() {
    return {
      restrict: 'A',

      controller: function ($scope) {
        $scope.openFancybox = function (url) {
          $.fancybox.open({
            href: url,
            type: 'iframe',
            width: 1280,
            height: 720,
            padding: 0,
            aspectRatio: true,
            closeBtn: true,
            openEffect: 'none',
            openSpeed: 400,
            closeEffect: 'none',
            closeSpeed: 400,
            helpers: {
              overlay: {
                closeClick: true,
                opacity: 0.5,
                showEarly: true,
                locked: true
              },
              media: {
                youtube: {
                  params: {
                    color: 'white',
                    modestbranding: 1,
                    theme: 'light',
                    showinfo: 0,
                    iv_load_policy: 3,
                    hd: 1,
                    vq: 'hd720'
                  }
                },
                vimeo: {
                  params: {
                    title: 0,
                    byline: 0,
                    portrait: 0,
                    color: 'ffffff',
                    autoplay: 1,
                    loop: false
                  }
                }
              }
            }
          });
        };
      }
    };
  }
})();