(function () {
  'use strict';

  angular
    .module('app.services')
    .service('ProductSelector', ProductSelector)
  ;

  ProductSelector.$inject = ['$http', '$q'];

  /*jshint newcap:false */
  function ProductSelector($http, $q) {
    var self = this;

    this.VERSION = 0.17; // db version
    this.timestamp = new Date().getTime();

    // private methods
    var initDb = function () {
      var deferred = $q.defer();

      // Initialise. If the database doesn't exist, it is created
      self.db = new localStorageDB("arcoProductSelector", localStorage);

      // Check if the database was just created. Useful for initial database setup
      if (self.db.isNew()) {
        createDb().then(function () {
          deferred.resolve();
        });
      }
      else {
        // check version
        var meta = self.db.query("meta")[0];
        var version = meta.version;

        if (version != self.VERSION) {
          dropDb();
          createDb().then(function () {
            deferred.resolve();
          });
        }
        else {
          deferred.resolve();
        }
      }

      return deferred.promise;
    };

    var createDb = function () {
      var deferred = $q.defer();

      self.db.createTable("meta", ["version"]);
      self.db.insert("meta", {version: self.VERSION});

      addProducts().then(function () {
        addResults().then(function () {
          deferred.resolve();
        });
      });

      return deferred.promise;
    };

    var dropDb = function () {
      self.db.drop();
      self.db = new localStorageDB("arcoProductSelector", localStorage);
    };

    var addProducts = function () {
      // columns are "code", "fabric", "name", "description", "colour", "base", "mid", "outer"

      return $http.get('data/products.json?' + self.timestamp)
        .then(function (res) {
          self.db.createTableWithData("products", res.data);
          self.db.commit();
        });
    };

    var addResults = function () {
      // columns are "layer1", "layer2", "layer3", "pain", "diff", "burn"

      return $http.get('data/results.json?' + self.timestamp)
        .then(function (res) {
          self.db.createTableWithData("results", res.data);
          self.db.commit();
        });
    };

    // public methods

    /**
     * Get all products
     * @returns {*}
     */
    this.allProducts = function () {
      return self.db.query("products", {
        sort: [
          ["name", "ASC"]
        ]
      });
    };

    /**
     * Get product by code
     * @param code
     * @returns {*}
     */
    this.getProduct = function (code) {
      return self.db.query("products", {code: code})[0];
    };

    /**
     * First we get a list of valid fabrics for these combinations
     * Then filter the list of products for the specified layer to only include those with invalid fabrics
     * @param layer
     * @param fabric1
     * @param fabric2
     */
    this.getInvalidProductCodesForLayer = function (layer, fabric1, fabric2) {

      var query;
      if (layer === 'base') {
        query = {'base': "true"};
      }
      else if (layer === 'mid') {
        query = {'mid': "true"};
      }
      else {
        query = {'outer': "true"};
      }

      var products = self.db.query("products", query);
      var validFabrics = this.getValidFabrics(layer, fabric1, fabric2, products);
      var invalidProductCodes = [];

      angular.forEach(products, function (product) {
        if (!_.contains(validFabrics, product.fabric)) {
          this.push(product.code);
        }
      }, invalidProductCodes);

      return invalidProductCodes;
    };

    /**
     * First we get a list of valid fabrics for these combinations
     * Then filter the list of products for the specified layer to only include those with valid fabrics
     * @param layer
     * @param fabric1
     * @param fabric2
     */
    this.getValidProductCodesForLayer = function (layer, fabric1, fabric2) {

      var query;
      if (layer === 'base') {
        query = {'base': "true"};
      }
      else if (layer === 'mid') {
        query = {'mid': "true"};
      }
      else {
        query = {'outer': "true"};
      }

      var products = self.db.query("products", query);
      var validFabrics = this.getValidFabrics(layer, fabric1, fabric2, products);
      var validProductCodes = [];

      angular.forEach(products, function (product) {
        if (_.contains(validFabrics, product.fabric)) {
          this.push(product.code);
        }
      }, validProductCodes);

      return validProductCodes;
    };

    /**
     * First we get all products that can be used for the specified layer
     * Then we determine the distinct fabrics that exist for these products
     * Then we check what combinations exist for these fabrics when combined with fabrics from the other layers
     * We end up with a list of valid fabrics for these combinations
     * @param layer
     * @param fabric1
     * @param fabric2
     * @param products
     */
    this.getValidFabrics = function (layer, fabric1, fabric2, products) {

      // get list of unique fabrics for the products
      var fabrics = [];
      angular.forEach(products, function (product) {
        this.push(product.fabric);
      }, fabrics);

      fabrics = _.uniq(fabrics);
      fabrics.sort();

      // remove selected fabrics from distinct list of fabrics
      fabrics = _.remove(fabrics, function (fabric) {
        return fabric !== fabric1;
      });

      if (fabric2 !== null) {
        fabrics = _.remove(fabrics, function (fabric) {
          return fabric !== fabric1;
        });
      }

      // now determine valid combinations
      var validCombinations,
        validFabrics = [];

      if (fabric2 !== null) {
        // for 3 layer combos, first check layer 1 and layer 2 combos
        validCombinations = self.db.query("results", function (row) {
          return row.layer1 === fabric1 && row.layer2 === fabric2 && _.contains(fabrics, row.layer3);
        });

        // now extract fabrics for these combinations for layer 3
        angular.forEach(validCombinations, function (combination) {
          this.push(combination.layer3);
        }, validFabrics);

        // then check layer 2 and layer 1 combos
        validCombinations = self.db.query("results", function (row) {
          return row.layer1 === fabric2 && row.layer2 === fabric1 && _.contains(fabrics, row.layer3);
        });

        // now extract fabrics for these combinations for layer 3
        angular.forEach(validCombinations, function (combination) {
          this.push(combination.layer3);
        }, validFabrics);

        // then check layer 1 and layer 3 combos
        validCombinations = self.db.query("results", function (row) {
          return row.layer1 === fabric1 && row.layer3 === fabric2 && _.contains(fabrics, row.layer2);
        });

        // now extract fabrics for these combinations for layer 2
        angular.forEach(validCombinations, function (combination) {
          this.push(combination.layer2);
        }, validFabrics);

        // then check layer 3 and layer 1 combos
        validCombinations = self.db.query("results", function (row) {
          return row.layer1 === fabric2 && row.layer3 === fabric1 && _.contains(fabrics, row.layer2);
        });

        // now extract fabrics for these combinations for layer 2
        angular.forEach(validCombinations, function (combination) {
          this.push(combination.layer2);
        }, validFabrics);

        // then check layer 2 and layer 3 combos
        validCombinations = self.db.query("results", function (row) {
          return row.layer2 === fabric1 && row.layer3 === fabric2 && _.contains(fabrics, row.layer1);
        });

        // now extract fabrics for these combinations for layer 1
        angular.forEach(validCombinations, function (combination) {
          this.push(combination.layer1);
        }, validFabrics);

        // then check layer 3 and layer 2 combos
        validCombinations = self.db.query("results", function (row) {
          return row.layer2 === fabric2 && row.layer3 === fabric1 && _.contains(fabrics, row.layer1);
        });

        // now extract fabrics for these combinations for layer 1
        angular.forEach(validCombinations, function (combination) {
          this.push(combination.layer1);
        }, validFabrics);
      }
      else {
        // for two layer combos, check layer 1 and layer 2
        validCombinations = self.db.query("results", function (row) {
          return row.layer1 === fabric1 && _.contains(fabrics, row.layer2);
        });

        // now extract fabrics for these combinations for layer 2
        angular.forEach(validCombinations, function (combination) {
          this.push(combination.layer2);
        }, validFabrics);

        // then check layer 2 and layer 1 combos
        validCombinations = self.db.query("results", function (row) {
          return row.layer2 === fabric1 && _.contains(fabrics, row.layer1);
        });

        // now extract fabrics for these combinations for layer 1
        angular.forEach(validCombinations, function (combination) {
          this.push(combination.layer1);
        }, validFabrics);

        // then check layer 1 and layer 3 combos
        validCombinations = self.db.query("results", function (row) {
          return row.layer1 === fabric1 && _.contains(fabrics, row.layer3);
        });

        // now extract fabrics for these combinations for layer 3
        angular.forEach(validCombinations, function (combination) {
          this.push(combination.layer3);
        }, validFabrics);

        // then check layer 3 and layer 1 combos
        validCombinations = self.db.query("results", function (row) {
          return row.layer3 === fabric1 && _.contains(fabrics, row.layer1);
        });

        // now extract fabrics for these combinations for layer 3
        angular.forEach(validCombinations, function (combination) {
          this.push(combination.layer1);
        }, validFabrics);
      }

      validFabrics = _.uniq(validFabrics);
      return validFabrics.sort();

    };

    /**
     * Get results for a single layer
     * @param fabric
     * @returns {*}
     */
    this.getSingleResults = function (fabric) {
      return self.db.query("results", {'layer1': fabric, 'layer2': '', 'layer3': ''})[0];
    };

    this.getCombinedResults = function (fabric1, fabric2, fabric3) {
      var results = self.db.query("results", {'layer1': fabric1, 'layer2': fabric2, 'layer3': fabric3});
      return results[0];

    };

    // We defer this, so that the JSON loads over http if required, before resolving
    this.init = function () {
      var deferred = $q.defer();
      initDb().then(function () {
        deferred.resolve();
      });

      return deferred.promise;
    };

    return this;
  }
})();