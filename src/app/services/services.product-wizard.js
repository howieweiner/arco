(function () {
  'use strict';

  angular
    .module('app.services')
    .service('ProductWizard', ProductWizard)
  ;

  ProductWizard.$inject = ['$rootScope', 'ProductSelector'];

  function ProductWizard($rootScope, productSelector) {

    return {
      // hold current selection
      selection: {
        base: null,
        mid: null,
        outer: null,
        complete: false,
        currentLayer: 'base',
        noSelectableOuterLayer: false
      },

      /**
       * reset wizard
       */
      restart: function () {
        this.selection.base = null;
        this.selection.mid = null;
        this.selection.outer = null;
        this.selection.complete = false;
        this.selection.currentLayer = 'base';
        this.selection.noSelectableOuterLayer = false;

        $rootScope.$broadcast('selectionReset');
      },

      /**
       * move wizard to next step/layer
       */
      next: function () {
        if (this.selection.currentLayer === 'base') {
          this.selection.currentLayer = 'mid';
        }
        else if (this.selection.currentLayer === 'mid') {
          this.selection.currentLayer = 'outer';
        }
        else if (this.selection.currentLayer === 'outer') {
          this.selection.currentLayer = 'outer';
          this.selection.complete = true;
        }
      },

      /**
       * determine if wizard is complete
       * @returns true if complete, false if not
       */
      complete: function () {
        return this.selection.complete;
      },

      /**
       * Add product for current layer
       * @param productCode
       */
      addProduct: function (productCode) {
        var product = productSelector.getProduct(productCode);

        if (this.selection.currentLayer === 'base') {
          this.selection.base = product;
        }
        else if (this.selection.currentLayer === 'mid') {
          this.selection.mid = product;
        }
        else {
          this.selection.outer = product;
        }

        $rootScope.$broadcast('selectionChanged');
      },

      /**
       * Clear product for a current layer
       */
      skip: function () {
        if (this.selection.currentLayer === 'base') {
          this.selection.base = null;
        }
        else if (this.selection.currentLayer === 'mid') {
          this.selection.mid = null;
        }
        else {
          this.selection.outer = null;
        }

        $rootScope.$broadcast('selectionSkipped');
      },

      /**
       *
       * @returns product details for currently selected product
       */
      current: function () {
        if (this.selection.currentLayer === 'base') {
          return this.selection.base;
        }
        else if (this.selection.currentLayer === 'mid') {
          return this.selection.mid;
        }
        else if (this.selection.currentLayer === 'outer') {
          return this.selection.outer;
        }
        else {
          return null;
        }
      },

      /**ns
       * @returns results for current/single layer based on the fabric
       */
      singleResults: function () {
        var current = this.current();
        if (current !== null) {
          return productSelector.getSingleResults(current.fabric);
        }
        else {
          return null;
        }
      },

      /**
       * returns details of current combination of layers
       * @returns {{pain: number, time: number, burn: number}}
       */
      combinedResults: function () {
        if (this.selection.base !== null || this.selection.mid !== null || this.selection.outer !== null) {
          // We could have anything from 1 to 3 fabrics for any layer. We need to determine which and pass
          // alphabetically
          var fabrics = [];

          if (this.selection.base !== null) {
            fabrics.push(this.selection.base.fabric);
          }
          else {
            fabrics.push('');
          }

          if (this.selection.mid !== null) {
            fabrics.push(this.selection.mid.fabric);
          }
          else {
            fabrics.push('');
          }

          if (this.selection.outer !== null) {
            fabrics.push(this.selection.outer.fabric);
          }
          else {
            fabrics.push('');
          }

          fabrics.sort(function (a, b) {
            if (a === "" && b === "") {
              return 0;
            }
            else if (a === "") {
              return 1;
            }
            else if (b === "") {
              return -1;
            }
            else {
              return a > b ? 1 : -1;
            }
          });

          return productSelector.getCombinedResults(fabrics[0], fabrics[1], fabrics[2]);
        }
        else {
          return null;
        }
      },

      /**
       * returns count of selected layers - used for image overlays
       * @returns {number}
       */
      productCount: function () {
        var count = 0;
        if (this.selection.base !== null) {
          count++;
        }

        if (this.selection.mid !== null) {
          count++;
        }

        if (this.selection.outer !== null) {
          count++;
        }

        return count;
      },

      getInvalidProductCodesForCurrentLayer: function () {
        var fabrics =  getLayerFabrics(this.selection.currentLayer, this.selection);
        var fabric1 = fabrics[0],
            fabric2 = fabrics[1];

        if (fabric1 !== null) {
          return productSelector.getInvalidProductCodesForLayer(this.selection.currentLayer, fabric1, fabric2);
        }
      },

      determineIfSelectableOuterLayer: function () {
        var fabrics =  getLayerFabrics('outer', this.selection);
        var fabric1 = fabrics[0],
            fabric2 = fabrics[1];

        var validProductCodes = productSelector.getValidProductCodesForLayer('outer', fabric1, fabric2);
        this.selection.noSelectableOuterLayer = validProductCodes.length === 0;
      },

      /**
       * determine iff current layer is outer, and more than one product has been selected and current selection returns
       * results.
       * Also, detect if mid layer, and no products are selected
       */
      noFinalResults: function() {
        if (this.selection.currentLayer === 'outer') {
          if(this.productCount() < 2) {
            return true;
          }
          else {
            var combined = this.combinedResults();
            return combined === undefined;
          }
        }
        else if (this.selection.currentLayer == 'mid') {
          if(this.productCount() < 1) {
            return true;
          }
        }

        return false;
      }
    };

    function getLayerFabrics(layer, selection) {
      var fabric1 = null,
        fabric2 = null;

      // if we have at least one product/fabric already selected, then we may need to check for valid combinations
      // if we are now viewing mid or outer layer, filter any invalid products based on combinations
      var invalidProductCodes = null;

      if (layer === 'mid') {
        // if base layer was not skipped, then check combinations against base layer fabric
        if (selection.base !== null) {
          fabric1 = selection.base.fabric;
        }
      }
      else if (layer === 'outer') {
        // if base layer was not skipped, then check combinations against base layer fabric
        if (selection.base !== null) {
          fabric1 = selection.base.fabric;

          // also check if mid layer was not skipped..
          if (selection.mid !== null) {
            fabric2 = selection.mid.fabric;
          }
        }
        // otherwise check if mid layer was not skipped..
        else if (selection.mid !== null) {
          fabric1 = selection.mid.fabric;
        }
      }

      return [fabric1, fabric2];
    }
  }

})();